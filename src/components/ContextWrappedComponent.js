import React, {
    useContext
} from 'react'

const HookedContext = React.createContext();
let ContextWrappedComponent = () => {
    return (
        <div>
            <HookedContext.Provider value="Yasir">
                <Greeting />
            </HookedContext.Provider>
        </div>
    )
}

let Greeting = () => {
    const value = useContext(HookedContext);
    return (
        <div>{`${value}, I'm using React Hooks`}</div>
    )
}
export default ContextWrappedComponent