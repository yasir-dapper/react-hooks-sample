import React, {
    useRef,
    useState
} from 'react'

export function Row() {
    const [name, setName] = useState('default')
    const nameRef = useRef();

    let submitButton = () => {
        setName(nameRef.current.value)
    }
    return (
        <div className="Row">
            <h3>useRef ({name})</h3>
            <input ref={nameRef} type="text" />
            <button onClick={submitButton} type="button"> Submit </button>
        </div>
    )
}