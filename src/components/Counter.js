import React, {
    useState,
    useEffect
} from 'react'

let Counter = () => {
    const [count, setCount] = useState(0)
   
    
    useEffect(() => {
        document.title = `${count} Clicks`
    })

    return (
        <div>
            <p>Count: {count}</p>
            <button onClick={() => setCount(0)}>Reset</button>
            <button onClick={() => setCount(count + 1)}>+</button>
            <button onClick={() => setCount(count - 1)}>-</button>
        </div>
    )
}

export default Counter