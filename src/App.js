import React, { Component } from 'react';
import './App.css';
import Counter from './components/Counter';
import ContextWrappedComponent from './components/ContextWrappedComponent';
import { Row } from './components/Row';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <Row 
            label="First Name" >
          <input />
          </Row> */}
          <h3>useState and useEffect</h3>
          <Counter />
          <hr />
          <h3>useContext</h3>
          <ContextWrappedComponent />
          <hr />
          <Row />
        </header>
      </div>
    );
  }
}

export default App;
